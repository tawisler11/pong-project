#-------------------------------------------------
#
# Project created by QtCreator 2013-04-14T13:23:18
#
#-------------------------------------------------

QT       += core gui

TARGET = Pong_Project
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    gameboard.cpp \
    aboutbox.cpp

HEADERS  += mainwindow.h \
    gameboard.h \
    aboutbox.h

FORMS    += mainwindow.ui \
    gameboard.ui \
    aboutbox.ui
