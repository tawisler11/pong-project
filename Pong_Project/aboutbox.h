#ifndef ABOUTBOX_H
#define ABOUTBOX_H

#include <QDialog>

namespace Ui {
class AboutBox;
}

class AboutBox : public QDialog
{
    Q_OBJECT
    
public:
    void mousePressEvent(QMouseEvent *);//will close when clicked
    explicit AboutBox(QWidget *parent = 0);
    ~AboutBox();
    
private:
    Ui::AboutBox *ui;//creates the ui pointer to the ui forms of AboutBox
};

#endif // ABOUTBOX_H
