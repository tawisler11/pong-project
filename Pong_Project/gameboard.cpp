#include "gameboard.h"
#include "ui_gameboard.h"
#include "mainwindow.h"
#include <QtGui/QtGui>
#include <cstdlib>
#include <ctime>

Gameboard::Gameboard(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Gameboard)
{
    this->x = 20;//starting position of left paddle
    this->y = 246;//starting position of left paddle
    this->x1 = 1180;//starting position of right paddle
    this->y1 = 246;//starting position of right paddle
    this->ballx = 595;//starting position of ball
    this->bally = 266;//starting position of ball
    this->score1 = 0;
    this->score2 = 0;
    ui->setupUi(this);

    this->rise = 0;//y speed of ball
    this->run = 5;//xspeed of ball

    randomDirection();//starting direction

    timer = new QTimer();
    timer->start(1);//timeout every milisecond
    connect(timer, SIGNAL(timeout()), this, SLOT(ballmove()));//move ball, check all boundries, and repaint

}

Gameboard::~Gameboard()
{
    delete ui;
}


void Gameboard::keyPressEvent(QKeyEvent *ke){
  if(rise!=0 && run!=0){ //if paused, don't move bars
    if (ke->key() == Qt::Key_W){//when w is pressed
        if(y > 21){
            y-=20;//move left bar up
        }
    }
    if (ke->key() == Qt::Key_S){//when s is pressed
        if(y < 471){
            y+=20;//move left bar down
        }

  }

    if (ke->key() == Qt::Key_O){//when o is pressed
        if(y1 > 21){
            y1-=20;//move right bar up
        }
    }
    if (ke->key() == Qt::Key_L){//when l is pressed
        if(y1 < 471){
            y1+=20;//move right bar down
        }
    }
  }//endpause

    if (ke->key() == Qt::Key_Space){ //pause when space is pressed, don't move ball
        if(run != 0 && rise != 0){
            runPause = run;
            risePause = rise;
            run = 0;
            rise = 0;
        }
        else if(run == 0 && rise ==0){
            run = runPause;
            rise = risePause;
        }
    }
    this->update();
}


void Gameboard::paintEvent(QPaintEvent *){
    QPainter paint(this);
    QImage pause("..\\Taylor\\pause.jpg");//pause image
    QImage backround("..\\Taylor\\Pong.jpg");//backround image
    QRect backroundRec(0,21,1200,500);//backround image size
    QRect pauseRec(408,156,450,175);//pause image size

    QPen pen(Qt::white);
    QBrush brush(Qt::green);
    QBrush brushr(Qt::red);
    QBrush brushb(Qt::blue);

    paint.drawImage(backroundRec,backround);//backround

    paint.setBrush(brushr);//redpaddle
    paint.drawRect(x,y,10,50);

    paint.setBrush(brushb);//bluepaddle
    paint.drawRect(x1,y1,10,50);

    paint.setBrush(brush);//greenball
    paint.setPen(pen);//gray outline
    paint.drawRoundedRect(ballx, bally, 10, 10, 25, 25);

    if(rise == 0 && run == 0){
        paint.drawImage(pauseRec,pause);//pause image
    }
}

void Gameboard::ballmove(){
    goal();//check if goal
    intersectLeftBar();//check if intersecting bar
    intersectRightBar();//check if intersecting bar
    boundary();//check if hitting boundry

    for(int x=2;x>0;x--){//change speed of game. change x and y based on current run and rise values
    ballx+=run;
    bally+=rise;
    }
    update();//repaint
}

void Gameboard::randomDirection(){//change direction, only called at the beggining
    srand(time(0));
//begining of direction selector
    if(rand()%2-1){
        directionX = true;
    }else{directionX=false;}

    if(rand()%2-1){
        directionY = true;
    }else{directionY=false;}
//end of direction selector
//direction selector has a 50% chance to make the ball go up or down, left or right

//choose rise speed
    while (rise < 1){
        rise = (rand()%10);
    }
//change direction start
    if(directionX){
        run=-run;
    }
    if(directionY){
        rise=-rise;
    }
//change direction end
//change direction based on direction selector outputs.
}

void Gameboard::randomRise(){//called after a goal is scored, only changes y direction
//direction selector
    if(rand()%2-1){
        directionY = true;
    }else{directionY=false;}
//rise speed
    while (rise < 1){
        rise = (rand()%10);
    }
//change direction
    if(directionY){
        rise=-rise;
    }
}


void Gameboard::goal(){
    if (ballx <= 15){
        //ball in center
        ballx = 595;
        bally = 266;
        //end of ball in center
        randomRise();//change rise value
        run=5;//send ball toward player who scored
        score2++;//increment score
        ui->score2->display(score2);//display score
    }
    else if (ballx >= 1190){
        //ball in center
        ballx = 595;
        bally = 266;
        //end of ball in center
        randomRise();//change rise value
        run=-5;//send ball toward player who scored
        score1++;//increment score
        ui->score1->display(score1);//display score
    }
}

void Gameboard::boundary(){//changes rise if ball touches boundries
    if (bally <=26 || bally >= 511){
        if (rise > 0){
            rise = -rise;
        }
        else if(rise < 0){
            rise = -rise;
        }
    }
}
void Gameboard::changeRise(){//called when ball touches a paddle. Does not change direction, only vertical speed.
    if(rise > 0){//keeps going down
        while (rise < 1){
            rise = (rand()%10);
        }
    }
    if(rise < 0){//keeps going up
        while (rise < 1){
            rise = (rand()%10);
        }
        rise=-rise;
    }
}

void Gameboard::intersectLeftBar(){//checks for bar intersection
    if (ballx <= x+15 && bally <= y+50 && bally >= y){
        run = -run;//reflects ball
        changeRise();//change speed of y
    }
}

void Gameboard::intersectRightBar(){//checks for bar intersection
    if (ballx >= x1-10 && bally <= y1+50 && bally >= y1){
        run = -run;//reflects ball
        changeRise();//change speed of y
    }
}

void Gameboard::on_actionAbout_triggered(){
    AboutBox *box = new AboutBox();
    box->show();
}

