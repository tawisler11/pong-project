#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include "aboutbox.h"
#include <QMainWindow>
#include <QPainter>
#include <QTimer>


namespace Ui {
class Gameboard;
}

class Gameboard : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Gameboard(QWidget *parent = 0);
    ~Gameboard();
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *event);
    void randomDirection();
    void changeRise();
    void randomRise();
    void boundary();
    void goal();
    void intersectRightBar();
    void intersectLeftBar();

    
private:
    Ui::Gameboard *ui;
    int x;
    int y;
    int x1;
    int y1;
    int ballx;
    int bally;
    int rise;
    int risePause;
    int run;
    int runPause;
    int score1;
    int score2;
    bool directionX;//not used yet - use rand%2 to decide if x and y are positive or negative.
    bool directionY;//not used yet
    QTimer *timer;
private slots:
    void ballmove();
    void on_actionAbout_triggered();
};

#endif // GAMEBOARD_H
