/*Authors: Ryan Willmann
           Taylor Wisler
  Purpose:
           This program will perform the game of pong with 2 players and a game that goes to the score of 5
  */

#include <QtGui/QApplication>
#include "mainwindow.h"
#include "gameboard.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}

