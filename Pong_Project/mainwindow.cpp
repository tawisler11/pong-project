#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->instruction->hide();//hides the label instruction
    ui->players->hide();//hides label players
    ui->control1->hide();//hides label of the controls of player 1
    ui->control2->hide();//hides label of the controls of player 2
    timer = new QTimer(this);
    timer2 = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(handleMainWindow()));//when timer times out then function handleMainWindow will be called
    connect(timer2, SIGNAL(timeout()), this, SLOT(handleClose()));//when timer2 times out then window will close
    timer->start(2500);
    timer2->start(7500);
}

void MainWindow::handleMainWindow(){//hides and shows labels
    ui->title->hide();
    ui->players->show();
    ui->instruction->show();
    ui->control1->show();
    ui->control2->show();

}
void MainWindow::handleClose(){//closes window
    this->close();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *){//opens Gameboard when MainWindow closes
    timer->stop();
    timer2->stop();
    Gameboard *g = new Gameboard();
    g->show();
}

void MainWindow::on_actionAbout_triggered(){
    AboutBox *box = new AboutBox();
    box->show();
}


