#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gameboard.h"
#include <QtGui/QtGui>
#include <QMainWindow>
#include "aboutbox.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    void closeEvent(QCloseEvent *);//checks when MainWindow closes
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    QTimer *timer;//timer for changing the window the first time
    QTimer *timer2;//timer for closing the window

private slots:
    void handleMainWindow();//after timer, changes the visible labels
    void handleClose();//closes the window after time2
    void on_actionAbout_triggered();
};

#endif // MAINWINDOW_H
