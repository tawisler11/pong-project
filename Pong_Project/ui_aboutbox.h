/********************************************************************************
** Form generated from reading UI file 'aboutbox.ui'
**
** Created: Wed Apr 24 17:30:32 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUTBOX_H
#define UI_ABOUTBOX_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_AboutBox
{
public:
    QLabel *label;

    void setupUi(QDialog *AboutBox)
    {
        if (AboutBox->objectName().isEmpty())
            AboutBox->setObjectName(QString::fromUtf8("AboutBox"));
        AboutBox->resize(400, 300);
        label = new QLabel(AboutBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(40, 50, 251, 161));
        QFont font;
        font.setPointSize(20);
        label->setFont(font);

        retranslateUi(AboutBox);

        QMetaObject::connectSlotsByName(AboutBox);
    } // setupUi

    void retranslateUi(QDialog *AboutBox)
    {
        AboutBox->setWindowTitle(QApplication::translate("AboutBox", "About", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AboutBox", "Creators:\n"
"      Taylor Wisler\n"
"      Ryan Willmann", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AboutBox: public Ui_AboutBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUTBOX_H
