/********************************************************************************
** Form generated from reading UI file 'gameboard.ui'
**
** Created: Wed Apr 24 17:40:11 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAMEBOARD_H
#define UI_GAMEBOARD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Gameboard
{
public:
    QAction *actionAbout;
    QWidget *centralwidget;
    QLCDNumber *score2;
    QLCDNumber *score1;
    QMenuBar *menubar;
    QMenu *menuAbout;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Gameboard)
    {
        if (Gameboard->objectName().isEmpty())
            Gameboard->setObjectName(QString::fromUtf8("Gameboard"));
        Gameboard->resize(1200, 521);
        actionAbout = new QAction(Gameboard);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        centralwidget = new QWidget(Gameboard);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        score2 = new QLCDNumber(centralwidget);
        score2->setObjectName(QString::fromUtf8("score2"));
        score2->setGeometry(QRect(475, 30, 200, 71));
        score2->setLayoutDirection(Qt::LeftToRight);
        score2->setFrameShape(QFrame::NoFrame);
        score1 = new QLCDNumber(centralwidget);
        score1->setObjectName(QString::fromUtf8("score1"));
        score1->setGeometry(QRect(389, 30, 200, 71));
        score1->setFrameShape(QFrame::NoFrame);
        score1->setSmallDecimalPoint(false);
        Gameboard->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Gameboard);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1200, 21));
        menubar->setDefaultUp(true);
        menubar->setNativeMenuBar(true);
        menuAbout = new QMenu(menubar);
        menuAbout->setObjectName(QString::fromUtf8("menuAbout"));
        Gameboard->setMenuBar(menubar);
        statusbar = new QStatusBar(Gameboard);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        Gameboard->setStatusBar(statusbar);

        menubar->addAction(menuAbout->menuAction());
        menuAbout->addAction(actionAbout);

        retranslateUi(Gameboard);

        QMetaObject::connectSlotsByName(Gameboard);
    } // setupUi

    void retranslateUi(QMainWindow *Gameboard)
    {
        Gameboard->setWindowTitle(QApplication::translate("Gameboard", "Pong", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("Gameboard", "About", 0, QApplication::UnicodeUTF8));
        menuAbout->setTitle(QApplication::translate("Gameboard", "About", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Gameboard: public Ui_Gameboard {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAMEBOARD_H
