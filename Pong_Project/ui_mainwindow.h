/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed Apr 24 17:32:11 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAbout;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *instruction;
    QLabel *title;
    QGridLayout *instructionlayout;
    QLabel *players;
    QLabel *control1;
    QLabel *control2;
    QStatusBar *statusBar;
    QMenuBar *About;
    QMenu *menuAbout;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(872, 500);
        QFont font;
        font.setFamily(QString::fromUtf8("Tandelle"));
        font.setPointSize(72);
        MainWindow->setFont(font);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionAbout->setCheckable(false);
        actionAbout->setEnabled(true);
        actionAbout->setIconVisibleInMenu(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        instruction = new QLabel(centralWidget);
        instruction->setObjectName(QString::fromUtf8("instruction"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Small Fonts"));
        instruction->setFont(font1);
        instruction->setTextFormat(Qt::AutoText);
        instruction->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        instruction->setTextInteractionFlags(Qt::NoTextInteraction);

        verticalLayout->addWidget(instruction);

        title = new QLabel(centralWidget);
        title->setObjectName(QString::fromUtf8("title"));
        title->setMaximumSize(QSize(1225, 16777215));
        title->setSizeIncrement(QSize(5, 9));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Small Fonts"));
        font2.setPointSize(72);
        font2.setBold(false);
        font2.setItalic(false);
        font2.setUnderline(false);
        font2.setWeight(50);
        font2.setStrikeOut(false);
        font2.setKerning(true);
        title->setFont(font2);
        title->setCursor(QCursor(Qt::ArrowCursor));
        title->setLayoutDirection(Qt::LeftToRight);
        title->setAutoFillBackground(false);
        title->setFrameShape(QFrame::NoFrame);
        title->setFrameShadow(QFrame::Plain);
        title->setTextFormat(Qt::AutoText);
        title->setScaledContents(false);
        title->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        title->setWordWrap(false);
        title->setMargin(0);
        title->setIndent(0);
        title->setOpenExternalLinks(false);

        verticalLayout->addWidget(title);

        instructionlayout = new QGridLayout();
        instructionlayout->setSpacing(6);
        instructionlayout->setObjectName(QString::fromUtf8("instructionlayout"));
        players = new QLabel(centralWidget);
        players->setObjectName(QString::fromUtf8("players"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Times New Roman"));
        font3.setPointSize(28);
        font3.setBold(true);
        font3.setWeight(75);
        players->setFont(font3);

        instructionlayout->addWidget(players, 0, 0, 1, 1);

        control1 = new QLabel(centralWidget);
        control1->setObjectName(QString::fromUtf8("control1"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Times New Roman"));
        font4.setPointSize(20);
        control1->setFont(font4);

        instructionlayout->addWidget(control1, 1, 0, 1, 1);

        control2 = new QLabel(centralWidget);
        control2->setObjectName(QString::fromUtf8("control2"));
        control2->setFont(font4);

        instructionlayout->addWidget(control2, 3, 0, 1, 1);


        verticalLayout->addLayout(instructionlayout);


        verticalLayout_2->addLayout(verticalLayout);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        About = new QMenuBar(MainWindow);
        About->setObjectName(QString::fromUtf8("About"));
        About->setGeometry(QRect(0, 0, 872, 21));
        menuAbout = new QMenu(About);
        menuAbout->setObjectName(QString::fromUtf8("menuAbout"));
        menuAbout->setTearOffEnabled(false);
        MainWindow->setMenuBar(About);

        About->addAction(menuAbout->menuAction());
        menuAbout->addAction(actionAbout);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
        instruction->setText(QApplication::translate("MainWindow", "Instructions", 0, QApplication::UnicodeUTF8));
        title->setText(QApplication::translate("MainWindow", "PONG", 0, QApplication::UnicodeUTF8));
        players->setText(QApplication::translate("MainWindow", "Player 1                        Pause                               Player 2", 0, QApplication::UnicodeUTF8));
        control1->setText(QApplication::translate("MainWindow", "Move bar up: w                          Space                                    Move bar up: o", 0, QApplication::UnicodeUTF8));
        control2->setText(QApplication::translate("MainWindow", "Move bar down: s                                                                  Move bar down: l", 0, QApplication::UnicodeUTF8));
        menuAbout->setTitle(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
